#!/bin/bash
echo "************************************* Jumping ***************************************"
if [ $# -eq 0 ]; 
then
	echo "WARNNING"
	#echo 'Arg1 - Original Name '
	echo 'Arg1 - New Name'
	#echo 'Arg3 - Git Repository Resource'
    exit 1
fi

new_name=$1
original_name="logistic-template"
repository="https://bitbucket.org/diegolirio/"$original_name

echo "New: "$new_name

rm -rf jumping
mkdir jumping
cd jumping

printf "\n"
echo "************************************ Cloning Repository ************************************"
git clone $repository

printf "\n"
echo "***************************** Replacing the contents of files ******************************"
#find . -type f | xargs sed -i  's/'$1'/'$2'/g'
find . -type f | xargs sed -i  's/'$original_name'/'$new_name'/g'

printf "\n"
echo "*********************************** Renaming directories ***********************************"
dirz=$(find . -depth -type d -iname '*'$original_name'*' | sort -r )
for i in $dirz
do
	#dirname=$(dirname $i)
	#mv $dirname'/'$original_name $dirname'/'$new_name
	find . -depth -type d -iname '*'$original_name'*' | sort -r | xargs rename 's@'$original_name'@'$new_name'@gi' {} + 2>/dev/null
done

echo "*************************************** git reset *****************************************"
cd $new_name
rm -f .git/index
git reset

echo ".......................................... done ..........................................."